import codecs
import os

from setuptools import find_packages, setup

###################################################################

NAME = "dyldstyle"
PACKAGES = find_packages(where="src")

CLASSIFIERS = [
    "Private :: Do Not Upload",
    "Intended Audience :: Developers",
]

INSTALL_REQUIRES = []


def read(*parts):
    """
    Build an absolute path from *parts* and and return the contents of the
    resulting file.  Assume UTF-8 encoding.
    """
    with codecs.open(
        os.path.join(os.path.abspath(os.path.dirname(__file__)), *parts), "rb", "utf-8"
    ) as f:
        return f.read()


if __name__ == "__main__":
    setup(
        name=NAME,
        description="dyldstyle wrangles bundles. (It's not a DJ!)",
        license="MIT",
        url="https://gitlab.com/adamwwolf/dyldstyle/",
        version="0.0.1-dev0",
        author="Adam Wolf",
        author_email="adamwolf@feelslikeburning.com",
        maintainer="Adam Wolf",
        maintainer_email="adamwolf@feelslikeburning.com",
        long_description=read("README.rst"),
        long_description_content_type="text/x-rst",
        packages=PACKAGES,
        package_dir={"": "src"},
        zip_safe=False,
        classifiers=CLASSIFIERS,
        install_requires=INSTALL_REQUIRES,
        entry_points={
            "console_scripts": ["wrangle-bundle=dyldstyle.bundle_wrangler:main"],
        },
        options={"bdist_wheel": {"universal": "1"}},
    )
