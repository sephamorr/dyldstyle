import argparse
import fileinput
import logging
import os
import shutil
import sys
from collections import defaultdict
from pathlib import Path
from typing import Optional

import itertools

from . import utils

logging.basicConfig(format="%(levelname)s:%(message)s")  # level=logging.DEBUG)


def get_shebang(path: Path) -> Optional[str]:
    with open(path, "rb") as f:
        line = f.readline().strip()

    if line.startswith("#!".encode("UTF-8")):
        return line.decode("UTF-8")
    else:
        return None


def check_python_caches(base_path: Path, make_fixes: bool):
    logging.info(f"Looking for Python caches in {base_path}")
    found_issues = False
    for path in base_path.rglob("*.pyc"):
        if make_fixes:
            logging.info(f"Removing cache {path}")
            path.unlink()
        else:
            logging.warning(f"Found cache {path}")
            found_issues = True
    return found_issues


def check_current_symlink(
    python_framework_path: Path, python_version: str, make_fixes: bool
):
    logging.info("Checking Python Version symlinks")

    versions_path = python_framework_path / "Versions"
    current_path = versions_path / "Current"

    found_issues = False

    if current_path.is_symlink() and os.readlink(current_path) == python_version:
        logging.debug(f"{current_path} is already the correct symlink.")
    else:
        if make_fixes:
            logging.info(f"Replacing Current with symlink to {python_version}")
            if current_path.exists():
                shutil.rmtree(current_path)
            os.symlink(python_version, current_path)
        else:
            logging.warning(
                f"Current symlink ({current_path}) "
                f"should be a symlink to {python_version}"
            )
            found_issues = True
    return found_issues


def get_fixed_shebang(
    script_dir_path: Path, python_framework_path: Path, relative_shebang_path: Path
) -> str:
    relocatable_shebang_template = """#!/bin/sh
'''exec' "$(dirname "$0")/{0}" "$0" "$@"
' '''
# the above calls the {0} interpreter relative to the directory of this script
"""
    # We can now make shebang path relative to the script directory
    shebang_path = python_framework_path / relative_shebang_path.as_posix()
    relative_interpreter_path = os.path.relpath(shebang_path, script_dir_path)
    logging.debug(f"Relative interpreter path is {relative_interpreter_path}")
    return relocatable_shebang_template.format(relative_interpreter_path)


def set_shebang(path: Path, shebang: str):
    with fileinput.FileInput(path, inplace=True, backup=".bak") as f:
        for line in f:
            if f.isfirstline():
                assert line.startswith("#!")
                print(shebang, end="")
            else:
                print(line, end="")


def resolve_subpath(dependency_path, base_directories):
    loader_prefix, subpath = split_loader_prefix(dependency_path)

    while str(subpath).startswith("../"):
        subpath = Path(str(subpath)[3:])

    if str(subpath).startswith("Frameworks/"):
        subpath = Path(str(subpath)[len("Frameworks/") :])

    if str(subpath) == "Contents/MacOS/Python":
        subpath = Path("Versions/Current/Python")
        # TODO can i do this more generally?

    if loader_prefix in ("@rpath", "@executable_path", "@loader_path"):
        resolved_path = None
        for base_directory in base_directories:
            potential_path = base_directory / subpath
            if potential_path.exists():
                return subpath
    return None


def get_all_files(path: Path):
    resolved_path = path.resolve()

    all_files = defaultdict(list)

    for p in resolved_path.glob("**/*"):
        if p.is_file():
            all_files[p.name].append(p)

    return all_files

def get_all_directories(path: Path):
    resolved_path = path.resolve()

    if resolved_path.is_dir():
        yield resolved_path

    for p in resolved_path.glob("**/*"):
        if p.is_dir():
            yield p

def handle_bundle(app_path: Path, python_version: str, fix: bool):
    python_framework_path = app_path / "Contents/Frameworks/Python.framework/"
    python_framework = str(python_framework_path)
    kicad_framework_path = app_path / "Contents/Frameworks/"

    issues = []

    if not app_path.exists():
        raise Exception(f"{app_path} was expected to exist.")
    if not python_framework_path.exists():
        raise Exception(f"{python_framework_path} was expected to exist.")
    if not kicad_framework_path.exists():
        raise Exception(f"{kicad_framework_path} was expected to exist.")

    found_issue = check_extra_python_symlink(app_path, fix)
    if found_issue:
        issues.append("Found extra Python symlink")

    found_issue = check_python_caches(python_framework_path, fix)
    if found_issue:
        issues.append("Found Python cache files")

    found_issue = check_current_symlink(python_framework_path, python_version, fix)
    if found_issue:
        issues.append("Python Framework Versions/Current is not a symlink")

    found_issue = check_shebangs(python_framework, python_framework_path, fix)
    if found_issue:
        issues.append("Found Python scripts with hardcoded shebangs.")

    macho_types = utils.get_macho_types(app_path)
    found_issue = check_executable_rpaths(
        macho_types["executable"], kicad_framework_path, python_framework_path, fix
    )
    if found_issue:
        issues.append("Found Mach-O executables without expected rpaths")

    all_files = get_all_files(app_path)

    has_dependency_issues = False
    for macho_items in macho_types.values():
        for macho_item in macho_items:
            if check_dependencies(
                macho_item,
                all_files,
                (kicad_framework_path, python_framework_path),
                python_version,
                app_path,
                fix,
            ):
                has_dependency_issues = True

    if has_dependency_issues:
        issues.append("Found dependency issues.")

    found_issue = check_signatures(itertools.chain(get_all_directories(app_path), itertools.chain.from_iterable(macho_types.values())), fix)
    if found_issue:
        issues.append("Found items with signatures.")

    return issues


def is_system_dependency(path: Path):
    if (
        str(path).startswith("/System/Library")
        or str(path).startswith("/usr/lib/libSystem")
        or str(path).startswith("/usr/lib")
    ):  # is this right?
        return True


def check_dependencies(
    item, all_files, rpath_base_paths, python_version, app_path: Path, make_fixes
):
    # this also checks to see if they're not bundled...

    # for all the non-system dependencies, let's make them relative
    # to one of the rpath's we're going to add to everything
    logging.info("Checking Mach-O dependencies")

    found_issue = False

    if str(item).endswith(".app/Contents/MacOS/Python") and not os.path.islink(item):
        #Confirm there is an extra copy of Python in the Frameworks folder
        traverse_to_other_python="../Frameworks/Python.framework/Python"
        replacement_python_path = os.path.abspath(os.path.join(os.path.dirname(item),traverse_to_other_python))
        if os.path.exists(replacement_python_path):
            #just use the other copy
            logging.info("Removing extra copy of Python binary and replacing with symlink")
            os.remove(item)
            os.symlink(traverse_to_other_python,item)
            found_issue = True
            return found_issue


    for dependency in utils.get_dependencies(item):

        loader_prefix, subpath = utils.split_loader_prefix(dependency)

        subpath = Path(subpath)

        if loader_prefix is None:
            if is_system_dependency(dependency):
                continue
            elif not subpath.is_absolute():
                if Path(item.parent, subpath).exists():
                    continue  # This is a local dependency, great!
                else:
                    logging.error(
                        f"Dependency {dependency} of {item} doesn't seem "
                        f"to be inside the app directory."
                    )
                    found_issue = True
            elif str(subpath).startswith(os.path.abspath(str(app_path))):
                logging.error(
                    f"Dependency {dependency} of {item} is not "
                    f"relative to the bundle."
                )
                found_issue = True

            elif subpath.is_absolute():
                logging.error(
                    f"Dependency {dependency} of {item} isn't "
                    f"inside the app directory."
                )
                found_issue = True

            else:
                logging.error(
                    f"Unexpected issue with dependency " f"{dependency} of {item}."
                )
                found_issue = True

            continue

        # this dependency had a loader prefix like @executable_path
        # Find the dependency, or the possible dependencies

        # If this has an rpath, let's see if we can just resolve it
        if loader_prefix == "@rpath":
            found = False
            for rpath in rpath_base_paths:  # dyldstyle.get_rpaths(item):
                if Path(dependency.replace("@rpath", str(rpath))).exists():
                    found = True
                    break
            if found:
                continue
            else:
                logging.warning(
                    f"{item}'s dependency {dependency} path started"
                    f"with @rpath but didn't seem resolvable with "
                    f"its rpaths."
                )
                found_issue = True
                continue

        if str(subpath).endswith("Contents/MacOS/Python"):
            subpath = Path(
                f"Versions/{python_version}/Python"
            )  #  TODO can we non-special case this?
            # It doesnt' seem to work with Current

        # TODO: rename this
        longest_specific_dealie = str(subpath).replace("../", "")

        matching_fullpaths = [
            fullpath
            for fullpath in all_files[subpath.name]
            if str(fullpath).endswith(longest_specific_dealie)
        ]

        if len(matching_fullpaths) == 0:
            logging.error(f"Unable to find {subpath.name}, " f"a dependency of {item}")
            found_issue = True
            continue

        if len(matching_fullpaths) > 1:
            logging.warning(
                f"Found more than one option for " f"resolving {dependency} on {item}."
            )
            # does this happen normally outside of Python?

        # Make them relative to the rpaths, and pick the "smallest one".
        resolutions = []
        for fullpath in matching_fullpaths:
            for rpath in rpath_base_paths:
                # should we use the rpaths already, rather than pass them in?
                resolutions.append(os.path.relpath(fullpath, start=rpath))

        if len(resolutions) == 0:
            logging.error(
                f"Unable to determine how to relativize {item}'s "
                f"dependency of {dependency}"
            )
            found_issue = True
            continue

        # pick the shortest one, I guess...
        resolutions.sort(key=len)
        resolution = resolutions[0]

        fixed_dependency = f"@rpath/{resolution}"

        if fixed_dependency == dependency:
            continue

        if make_fixes:
            logging.info(
                f"Changing {item}'s dependency of {dependency} "
                f"to {fixed_dependency}"
            )
            utils.change_dependency(item, dependency, fixed_dependency)
        else:
            logging.warning(
                f"{item}'s dependency of {dependency} should " f"be {fixed_dependency}"
            )
            found_issue = True

    return found_issue

def check_signatures(signable_things, make_fixes):
    logging.info("Checking for signable things with signatures")
    # If things are signed and then we edit them, the signatures are invalid.
    # Until we add signing to packaging, we should remove all the signatures.

    found_issue = False
    for signable_thing in signable_things:
        if utils.get_signature(signable_thing) is not None:
            if make_fixes:
                logging.info(f"Removing signature on {signable_thing}")
                utils.remove_signature(signable_thing)
            else:
                logging.warning(f"Found signature on {signable_thing}")
                found_issue = True
    return found_issue

def check_shebangs(python_framework, python_framework_path, make_fixes):
    logging.info("Checking for hardcoded Python shebangs")
    bindir = python_framework_path / "Versions/Current/bin"

    found_issue = False

    for python_script_path in bindir.rglob("*"):
        if python_script_path.name.endswith(".bak"):
            continue

        shebang = get_shebang(python_script_path)
        found_issue = False

        if shebang:
            if shebang.startswith("#!/bin/sh"):
                # We are going to assume we previously fixed this one
                continue

            fixed_shebang = None

            # TODO: do we need this?

            if "python" not in shebang:
                pass
            else:
                pass

                # fixed_shebang = get_fixed_shebang(
                #     script_dir_path=python_script_path.parent,
                #     relative_shebang_path=relative_shebang_path,
                #     python_framework_path=python_framework_path,
                # )
                # if fixed_shebang is None:
                #     pass
                #
                # if fixed_shebang is not None:
                #     if make_fixes:
                #         logging.info(
                #             f"Fixing {python_script_path}'s shebang from {shebang} to {fixed_shebang}"
                #         )
                #         set_shebang(python_script_path, fixed_shebang)
                #     else:
                #         logging.warning(
                #             f"{python_script_path} has a shebang of {shebang} and it should be {fixed_shebang}"
                #         )
                #         found_issue = True

    return found_issue


def check_extra_python_symlink(kicad_app_path, make_fixes):
    logging.info("Checking for extra Python symlink")
    extra_python = kicad_app_path / "Contents/MacOS/Python"
    if extra_python.exists():
        if make_fixes:
            logging.info(f"Removing extra Python symlink at {extra_python}")
            extra_python.unlink(extra_python)
            return False
        else:
            logging.warning(f"Extra Python symlink found at {extra_python}")
            return True

def check_executable_rpaths(
    executables, kicad_framework_path, python_framework_path, make_fixes
):
    executables_rpaths = defaultdict(list)
    found_issue = False

    for executable in executables:
        rpaths = utils.get_rpaths(executable)
        executables_rpaths[executable] = rpaths

        python_framework_relpath = os.path.relpath(
            python_framework_path, executable.parent
        )
        python_framework_rpath = f"@executable_path/{python_framework_relpath}"

        kicad_framework_relpath = os.path.relpath(
            kicad_framework_path, executable.parent
        )
        kicad_framework_rpath = f"@executable_path/{kicad_framework_relpath}"

        for desired_rpath in (python_framework_rpath, kicad_framework_rpath):
            if desired_rpath not in rpaths:
                if make_fixes:
                    logging.info(f"Adding rpath {desired_rpath} " f"to {executable}")
                    utils.add_rpath(executable, desired_rpath)
                else:
                    logging.warning(
                        f"Did not find desired rpath of {desired_rpath} "
                        f"in {executable}'s rpaths of {rpaths}"
                    )
                    found_issue = True
    return found_issue


def get_executable_rpaths(executables):
    executables_rpaths = defaultdict(list)
    for executable in executables:
        rpaths = utils.get_rpaths(executable)
        executables_rpaths[executable] = rpaths
    return executables_rpaths


def parse_args():
    parser = argparse.ArgumentParser(
        description="Check bundling issues for macOS apps with "
        "embedded Python frameworks"
    )
    path_arg = parser.add_argument("path", help="foo.app/ directory")
    parser.add_argument(
        "--python-version",
        dest="python_version",
        help="python version, in the form of x.y",
        required=True,
    )

    parser.add_argument(
        "--fix", action="store_true", default=False, help="make changes"
    )
    parser.add_argument("--verbose", "-v", action="count", default=0)

    args = parser.parse_args()

    logger = logging.getLogger()
    if args.verbose == 0:
        logger.setLevel(logging.WARN)
    elif args.verbose == 1:
        logger.setLevel(logging.INFO)
    elif args.verbose >= 2:
        logger.setLevel(logging.DEBUG)

    if not Path(args.path).is_dir():
        raise argparse.ArgumentError(
            path_arg, f"{args.path} does not seem to be an existing directory"
        )

    return args


def main():
    args = parse_args()
    app_path = Path(args.path)

    if args.fix:
        print(f"Looking for issues in {app_path}, and fixing what we can...")
        handle_bundle(app_path, args.python_version, fix=True)
        # If we fixed things, let's recheck everything,
        # and make sure it's all fixed!

    print(f"Checking for bundle issues in {app_path}...")
    issues = handle_bundle(app_path, args.python_version, fix=False)
    if issues:
        print("Issues found: ")
        for issue in issues:
            print(f"\t{issue}")
        sys.exit(1)
    else:
        logging.info("No issues found.")
        sys.exit()


if __name__ == "__main__":
    main()
