import argparse
import json
import logging
import subprocess
import sys
from itertools import chain
from pathlib import Path
from typing import List, Optional, Union

logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)

def get_signature(path: Union[str, Path]) -> Optional[str]:
    cmd = ["/usr/bin/codesign", "-d", str(path)]
    completed_process = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    lines = completed_process.stdout.decode("utf-8").splitlines()

    if lines[0].startswith("Executable="):
        return "\n".join(lines)
    else:
        return None

def remove_signature(path: Union[str, Path]) -> Optional[str]:
    cmd = ["/usr/bin/codesign", "--remove-signature", str(path)]
    completed_process = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    lines = completed_process.stdout.decode("utf-8").splitlines()

    if completed_process.returncode == 0:
        return
    else:
        logging.warning(f"Tried to remove signature on {path}.  codesign returned non-zero and said: {lines}")

def get_install_name(path: Union[str, Path]) -> Optional[str]:
    # are these actually install name *ID*s?
    """Returns the install_name of a shared library"""
    cmd = ["/usr/bin/otool", "-D", str(path)]
    lines = (
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        .decode("utf-8")
        .splitlines()
    )
    if not lines:
        return None
    elif lines[0].endswith("is not an object file"):
        return None
    elif lines[0].endswith(":"):
        if len(lines) == 1:
            return None
        if len(lines) == 2:
            return lines[1]

    if lines[0].startswith("Archive:"):
        logging.warning(
            f"otool -l returned unexpected output beginning with Archive: {lines[0]}"
        )
        return None
    return None


def set_install_name(path: Union[str, Path], id: str):
    cmd = ["install_name_tool", "-id", id, str(path)]
    lines = (
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        .decode("utf-8")
        .splitlines()
    )


def change_dependency(path: Union[str, Path], old: str, new: str, verify=True):
    cmd = ["install_name_tool", "-change", old, new, str(path)]
    lines = (
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        .decode("utf-8")
        .splitlines()
    )

    if verify:
        dependencies = get_dependencies(path)
        ok = old not in dependencies and new in dependencies
        if not ok:
            logging.debug(
                f"Tried to change dependency of {path}, and it appears to have failed."
            )
            if old == get_install_name(path):
                # ahh, it's actually the install name, ok... what?!
                logging.debug(
                    f"The 'dependency' is actually its own install name.  Changing."
                )
                set_install_name(path, new)

                dependencies = get_dependencies(path)
                ok = old not in dependencies and new in dependencies
                if ok:
                    return

            logging.error(f"Error changing dependency of {path} from {old} to {new}")


def get_dependencies(path: Union[str, Path]) -> List[str]:
    # The first thing that comes through here might be the actual library, in form of its ID

    dependencies = []

    cmd = ["/usr/bin/otool", "-L", str(path)]
    lines = (
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        .decode("utf-8")
        .splitlines()
    )  # TODO: stderr?

    if len(lines) > 1:
        for line in lines[1:]:
            line = line.lstrip()
            if any(i in line for i in ["(architecture x86_64)","(architecture arm64)"]):
                continue
            tail = line.find(" (compatibility ")
            if tail != -1:
                line = line[0:tail]
            dependencies.append(line)
    return dependencies


def add_rpath(path: Union[str, Path], rpath: str):
    cmd = ["/usr/bin/install_name_tool", "-add_rpath", rpath, str(path)]
    lines = (
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        .decode("utf-8")
        .splitlines()
    )


def get_rpaths(path: Path) -> List[str]:
    rpaths = []
    cmd = ["/usr/bin/otool", "-l", str(path)]
    lines = (
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        .decode("utf-8")
        .splitlines()
    )
    for index, line in enumerate(lines):
        if line.endswith("LC_RPATH") and line.lstrip().startswith("cmd "):
            rpath_line = lines[index + 2].lstrip()  # TODO handle last line
            if not rpath_line.startswith("path "):
                raise Exception(f"Unexpected output from otool: {lines}")
            end_index = rpath_line.find(" (offset")  # TODO what it if doesn't find it?
            rpath = rpath_line[5:end_index]
            rpaths.append(rpath)
    return rpaths


def get_file_magic(path) -> str:
    cmd = ["/usr/bin/file", "-b", path]
    lines = (
        subprocess.check_output(cmd, stderr=subprocess.STDOUT)
        .decode("utf-8")
        .splitlines()
    )
    return lines[0]


def get_install_names(dylibs):
    install_names = {}

    for dylib in dylibs:
        install_name = get_install_name(dylib)
        install_names[dylib] = install_name

        # is this weird? does it need to get changed?
        # look in everything else's dependencies for this
    return install_names


def split_loader_prefix(path):
    found_prefix = None

    valid_prefixes = ("@rpath", "@loader_path", "@executable_path")
    for prefix in valid_prefixes:
        if str(path).startswith(prefix + "/"):
            found_prefix = prefix
            break

    if found_prefix is None:
        rest_of_path = path
        if path.startswith("@"):
            logging.warning(
                f"Unexpected loader prefix in {path}, expected one of {valid_prefixes}"
            )
    else:
        if len(path) > len(found_prefix) + 1:  # the +1 is for the /
            rest_of_path = Path(str(path)[len(found_prefix) + 1 :])
        else:
            rest_of_path = None

    return found_prefix, rest_of_path


def get_macho_type(path):
    # should we use `otool -hv some_file` instead of magic/file?

    if not path.is_file():
        return
    if path.is_symlink():
        return

    magic = get_file_magic(path)
    if not magic.startswith("Mach-O "):
        return

    if " executable" in magic:
        logging.debug(f"Found Mach-O executable: {path} with magic of {magic}")
        return "executable"
    elif " library" in magic:
        logging.debug(f"Found Mach-O library: {path} with magic of {magic}")
        return "dylib"
    elif " bundle" in magic:
        logging.debug(f"Found Mach-O bundle: {path} with magic of {magic}")
        return "bundle"
    elif " object" in magic:
        logging.debug(f"Found Mach-O object: {path} with magic of {magic}")
        return "object"
    else:
        logging.warning(
            f"Found Mach-O file with unknown type: {path} with magic of {magic}"
        )


def get_macho_types(path):
    # TODO make sure this works with either a file or a dir

    machos = {"executable": [], "object": [], "dylib": [], "bundle": []}
    resolved_path = path.resolve()
    macho_magic_numbers = set([bytes.fromhex("cafebabe"), bytes.fromhex("feedface"), bytes.fromhex("feedfacf"), bytes.fromhex('cffaedfe')])
    for p in resolved_path.glob("**/*"):
        if p.is_file():
            f = open(p, "rb")
            v = f.read(4)
            if v not in macho_magic_numbers:
                continue
        macho_type = get_macho_type(p)
        if macho_type is not None:
            machos[macho_type].append(p)
    return machos


def parse_args():
    parser = argparse.ArgumentParser(description="")
    parser.add_argument("--verbose", "-v", action="count", default=0)
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--print-install-names", action="store_true", default=True)
    group.add_argument(
        "--no-print-install-names", action="store_false", dest="print_install_names"
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--print-dependencies", action="store_true", default=True)
    group.add_argument(
        "--no-print-dependencies", action="store_false", dest="print_dependencies"
    )
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--print-rpaths", action="store_true", default=True)
    group.add_argument("--no-print-rpaths", action="store_false", dest="print_rpaths")

    parser.add_argument("path")

    args = parser.parse_args()

    logger = logging.getLogger()
    if args.verbose == 0:
        logger.setLevel(logging.WARN)
    elif args.verbose == 1:
        logger.setLevel(logging.INFO)
    elif args.verbose >= 2:
        logger.setLevel(logging.DEBUG)

    return args


def default_json_serializer(obj):
    if isinstance(obj, Path):
        return str(obj)
    elif isinstance(obj, PosixPath):
        return str(obj)
    else:
        raise TypeError


def main():
    args = parse_args()
    path = Path(args.path)
    if path.is_dir():
        print(f"Looking at {path}")
    else:
        print(f"Looking under {path} for Mach-O files")
    macho_types = get_macho_types(path)

    output = {}
    for key, values in macho_types.items():
        for value in values:
            output[value] = {"macho_type": key}

    if args.print_rpaths:
        for path in macho_types["executable"]:
            output[path]["rpaths"] = get_rpaths(path)

    if args.print_dependencies:
        for path in chain(macho_types["dylib"], macho_types["executable"]):
            output[path]["dependencies"] = get_dependencies(path)

    if args.print_install_names:
        for path in macho_types["dylib"]:
            output[path]["install_name"] = get_install_name(path)

    # what about bundles and objects?
    # TODO: ? Python.o doesn't seem to have an install name id set, or dependencies

    for key, values in macho_types.items():
        for value in values:
            output[str(value)] = output.pop(value)

    print(json.dumps(output, sort_keys=True, indent=4))

    logging.info("Done.")
    sys.exit()


if __name__ == "__main__":
    main()
